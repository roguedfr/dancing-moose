/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {
	
	// Remove empty P tags created by WP inside of Accordion and Orbit
	jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
		  jQuery(this).wrap("<div class='widescreen responsive-embed'/>");
		} else {
		  jQuery(this).wrap("<div class='responsive-embed'/>");
		}
	});


  jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();

    if (scroll >= 100) {
      jQuery("body").addClass("scrolled");
    } else {
      jQuery("body").removeClass("scrolled");
    }
  });

  jQuery.fn.isOnScreen = function() {
    var win = jQuery(window);

    var viewport = {
      top: win.scrollTop(),
      left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    return !(
      viewport.right < bounds.left ||
      viewport.left > bounds.right ||
      viewport.bottom < bounds.top ||
      viewport.top > bounds.bottom
    );
  };

  jQuery(".slide-up, .slide-down, .slide-right, .slow-fade, .col__circle-list ul li").each(function() {
    if (jQuery(this).isOnScreen()) {
      jQuery(this).addClass("active");
    }
  });

  // ========== Add class on entering viewport

  jQuery.fn.isInViewport = function() {
    var elementTop = jQuery(this).offset().top;
    var elementBottom = elementTop + jQuery(this).outerHeight();
    var viewportTop = jQuery(window).scrollTop();
    var viewportBottom = viewportTop + jQuery(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  jQuery(window).on("resize scroll", function() {
    jQuery(".slide-up, .slide-down, .slide-right, .slow-fade, .col__circle-list ul li").each(function() {
      if (jQuery(this).isInViewport()) {
        jQuery(this).addClass("active");
      }
    });
  });



jQuery(function(jQuery){ // use jQuery code inside this to avoid "jQuery is not defined" error
  jQuery('.blog_loadmore').click(function(){
 
    var button = jQuery(this),
        data = {
      'action': 'loadmore',
      'query': posts_myajax, // that's how we get params from wp_localize_script() function
      'page' : current_page_myajax
    };

    jQuery.ajax({ // you can also use jQuery.post here
      url : '/wp-admin/admin-ajax.php', // AJAX handler
      data : data,
      type : 'POST',
      beforeSend : function ( xhr ) {
        button.find('span').text('Loading...'); // change the button text, you can also add a preloader image
      },
      success : function( data ){
        if( data ) { 
          button.text( 'More posts' ).before(data); // insert new posts
          current_page_myajax++;
 
          if ( current_page_myajax == max_pages_myajax ) 
            button.remove(); // if last page, remove the button
          // you can also fire the "post-load" event here if you use a plugin that requires it
          // jQuery( document.body ).trigger( 'post-load' );
        } else {
          button.remove(); // if no data, remove the button as well
        }
      },
    });
  });
});

});

jQuery(document).ready(function(){
  jQuery('.hero-carousel').slick({
  	infinite: true,
  	arrows: true,
  	autoplay: true,
  	autoplaySpeed:8000,
  	speed: 800,
    dots: false,
  	nextArrow: '<i class="fas fa-chevron-right slickNext"></i>',
  	prevArrow: '<i class="fas fa-chevron-left slickPrev"></i>',
  });
});

jQuery(document).ready(function(){
  jQuery('.testimonial_carousel').slick({
  	infinite: true,
	  speed: 500,
    arrows: false,
	  fade: true,
	  autoplay: true,
  	autoplaySpeed:8000,
  });
});

jQuery(document).ready(function(){
  jQuery('.offers-carousel').slick({
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed:3000,
  });
});

jQuery(document).ready(function(){
  jQuery('.announcements-carousel').slick({
    infinite: true,
    speed: 500,
    fade: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed:3000,
  });
});

jQuery(function(){
    jQuery(".locationGallery__image").SmartPhoto();
});


jQuery('#choose-moose').on('click', function(e){
  e.preventDefault();
  var chooseSelect = jQuery( ".chooseSelect" ).val();
  window.location.href = chooseSelect;
});
