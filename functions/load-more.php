<?php


function misha_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
 
				$image = get_field('hero_image');?>
				<div class="cell small-12 medium-6 large-4 pb2 blog__container">
					<a href="<?php the_permalink();?>" class="blog">
						<div class="blog__background" style="background-image: url(<?php echo $image['url'];?>)"></div>
						<h4 class="heading heading__xl heading__white heading__caps pb3 pt3 pl4 pr2 text-center blog__title"><?php the_title();?></h4>
						<div class="blog__content pl1 pr1 text-center">
							<?php the_field('hero_content');?>
						</div>
						<span class="hero-carousel__button btn btn__black btn__version3 blog__button"><?php get_template_part( 'assets/svg/button3' ); ?><span class="heading heading__md heading__caps">Read More</span></span>
					</a>
				</div>
 
 
		<?php endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
 
 
 
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}