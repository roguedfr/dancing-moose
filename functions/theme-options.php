<?php
add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Register options page.
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Theme General Settings'),
            'menu_title'    => __('Theme Settings'),
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));

        $option_page = acf_add_options_page(array(
            'page_title'    => __('Menus'),
            'menu_title'    => __('Menus'),
            'menu_slug'     => 'menu-settings',
            'capability'    => 'edit_posts',
            'position'      => '10',
            'icon_url'      => 'dashicons-carrot',
            'redirect'      => false
        ));

        $option_page = acf_add_options_page(array(
            'page_title'    => __('Offers'),
            'menu_title'    => __('Offers'),
            'menu_slug'     => 'offers-settings',
            'capability'    => 'edit_posts',
            'position'      => '12',
            'icon_url'      => 'dashicons-megaphone',
            'redirect'      => false
        ));

        $option_page = acf_add_options_page(array(
            'page_title'    => __('Announcements'),
            'menu_title'    => __('Announcements'),
            'menu_slug'     => 'announcements-settings',
            'capability'    => 'edit_posts',
            'position'      => '11',
            'icon_url'      => 'dashicons-megaphone',
            'redirect'      => false
        ));
    }
}