<?php
function my_acf_init() {
    acf_update_setting('google_api_key',  get_field('google_api_key', 'options'));
}
add_action('acf/init', 'my_acf_init');