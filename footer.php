<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
				<section class="pt2 pb2 section__primary section">
					<div class="grid-container">
						<div class="grid-x grid-margin-x">
							<div class="cell small-12 medium-6 text-sm-center text-right">
				 				<h3 class="heading heading__lg heading__caps heading__white">Join the club</h3>
				 			</div>
				 			<div class="cell small-12 medium-6 text-sm-center text-left">
				 				<?php if( have_rows('newsletter_button', 'options') ):
									while( have_rows('newsletter_button', 'options') ): the_row(); ?>
										<?php get_template_part( 'parts/_button' ); ?>
								<?php endwhile; endif;?>
							</div>
						</div>
					</div>
				</section>

				<footer class="footer pt4 pb4 mt0" role="contentinfo">

					<div class="grid-container">
					
						<div class="inner-footer grid-x grid-margin-x grid-padding-x align-middle">
							<div class="small-12 cell pb1 text-center">
								<div class="footer__moose pb1"><?php get_template_part( 'assets/svg/moose-head' ); ?></div>
								<h4 class="heading heading__md heading__caps heading__white"><?php the_field('slogan', 'options');?></h4>
							</div>

							<div class="small-12 large-3 cell text-center text-lg-left mb1 medium-order-3 large-order-1">
								<p class="source-org copyright mb0">Copyight <?php bloginfo('name'); ?> &copy; <?php echo date('Y'); ?></p>
							</div>
							
							<div class="small-12 large-6 cell mb1 medium-order-2 large-order-2">
								<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav>
		    				</div>
							
							<div class="small-12 medium-12 large-3 cell text-center text-lg-right mb1 medium-order-1 large-order-3" id="social">
								<?php if( have_rows('social', 'options') ):
 									while( have_rows('social', 'options') ): the_row(); ?>
 										<a href="<?php the_sub_field('link');?>" title="<?php the_sub_field('title');?>">
	 										<i class="fab <?php the_sub_field('icon');?>"></i>
	 									</a>
 								<?php endwhile; endif;?>
							</div>
						
						</div> <!-- end #inner-footer -->

					</div>
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->


		<div class="reveal" id="bookingModal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
		  	<div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
		</div>
		
		<?php wp_footer(); ?>

		

		<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/DancingMoose/17449?includeJquery=false" type="hidden">

		<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
		
	</body>
	
</html> <!-- end page -->