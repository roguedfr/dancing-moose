<?php
/**
 * ============== Template Name: Booking Page
 */
get_header();?>

<?php $heroImage = get_field('hero_image');?>
<section>
	<div class="h50 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		<div class="grid-container hero-carousel__content">
		  	<div class="grid-x text-center align-middle h50">
		  		<div class="cell small-offset-2 small-8">
		  			<h2 class="heading heading__white heading__xxl heading__alt-font pt2"><?php the_title();?></h2>
		  			<h6 class="heading heading__white heading__md heading__body-font pb2"><?php the_field('hero_content');?></h6>
		  			<?php if( have_rows('booking_button') ):
						while( have_rows('booking_button') ): the_row(); ?>
							<?php get_template_part( 'parts/_button' ); ?>
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="#choose"></div>
<?php get_template_part( 'parts/locations', 'home' ); ?>

<?php get_footer(); ?>