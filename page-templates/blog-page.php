<?php
/**
 * ============== Template Name: Blog Page
 */
get_header();?>

<?php $heroImage = get_field('hero_image');?>
<section>
	<div class="h75 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		<div class="grid-container hero-carousel__content">
		  	<div class="grid-x text-center align-middle h75">
		  		<div class="cell small-offset-2 small-8">
		  			<h2 class="heading heading__white heading__xxl heading__alt-font pt2"><?php the_title();?></h2>
		  			<h6 class="heading heading__white heading__md heading__body-font pb2"><?php the_field('hero_content');?></h6>
		  			<?php if( have_rows('booking_button') ):
						while( have_rows('booking_button') ): the_row(); ?>
							<?php get_template_part( 'parts/_button' ); ?>
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pt5 pb5">
	<div class="grid-container">
		<div class="grid-x grid-margin-x" id="load-more-container">

			<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
				    'post_type' => 'post',
	    			'post__in' => array(get_field('featured_blog', 'options'))
				);
				
	    		$featured = new WP_Query( $args );
	    		if ( $featured->have_posts() ) :
	        	while ( $featured->have_posts() ) : $featured->the_post(); ?>
	        		<?php $image = get_field('hero_image');?>
				<div class="cell small-12 pb2 blog__featured slide-up">
					<a href="<?php the_permalink();?>" class="blog">
						<div class="blog__background-featured" style="background-image: url(<?php echo $image['url'];?>)"></div>
						<h4 class="heading heading__xl heading__white heading__caps pb3 pt3 pl4 pr2 text-center blog__title-featured"><?php the_title();?></h4>
						<div class="blog__content pl1 pr1 text-center">
							<?php the_field('hero_content');?>
						</div>
						<span class="hero-carousel__button btn btn__black btn__version3 blog__button"><?php get_template_part( 'assets/svg/button3' ); ?><span class="heading heading__md heading__caps">Read More</span></span>
					</a>
				</div>
			<?php endwhile;endif?>

			<?php wp_reset_postdata();?>

			<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args = array(
				    'post_type' => 'post',
	    			'posts_per_page' => 6,
	    			'paged' => $paged,
	    			'post__not_in' => array(get_field('featured_blog', 'options'))
				);
				
	    		$loop = new WP_Query( $args );
	    		if ( $loop->have_posts() ) :
	        	while ( $loop->have_posts() ) : $loop->the_post(); ?>
	        		<?php $image = get_field('hero_image');?>
				<div class="cell small-12 medium-6 large-4 pb2 blog__container slide-up">
					<a href="<?php the_permalink();?>" class="blog">
						<div class="blog__background" style="background-image: url(<?php echo $image['url'];?>)"></div>
						<h4 class="heading heading__xl heading__white heading__caps pb3 pt3 pl4 pr2 text-center blog__title"><?php the_title();?></h4>
						<div class="blog__content pl1 pr1 text-center">
							<?php the_field('hero_content');?>
						</div>
						<span class="hero-carousel__button btn btn__black btn__version3 blog__button"><?php get_template_part( 'assets/svg/button3' ); ?><span class="heading heading__md heading__caps">Read More</span></span>
					</a>
				</div>
			<?php endwhile; endif?>

			<?php
			// don't display the button if there are not enough posts
			if (  $loop->max_num_pages > 1 ){;?>
				<div class="blog_loadmore cell small-12 text-center">
					<a class="hero-carousel__button btn btn__red btn__version4"><?php get_template_part( 'assets/svg/button4'); ?><span class="heading heading__md heading__caps">Load more</span></a>
				</div>
			<?php }
			?>
			<?php wp_reset_postdata();?>
		</div>
	</div>
</section>
<?php get_template_part( 'parts/choose-moose'); ?>

<script>
// {ID} is any unique name, example: b1, q9, qq, misha etc, it should be uniq
// ^ the tip above is confusing. What you need is just unique variable names 
var posts_myajax = '<?php echo serialize( $loop->query_vars ) ?>',
    current_page_myajax = 1, 
    max_page_myajax = <?php echo $loop->max_num_pages ?>
</script>
<?php get_footer(); ?>