<?php
/**
 * ============== Template Name: Home
 */
get_header();?>

<?php get_template_part( 'parts/hero', 'home' ); ?>

<?php get_template_part( 'parts/offers' ); ?>

<?php get_template_part( 'parts/video', 'home' ); ?>

<?php get_template_part( 'parts/menus' ); ?>

<?php get_template_part( 'parts/nearest' ); ?>

<?php get_template_part( 'parts/locations', 'home' ); ?>

<?php get_template_part( 'parts/choose-moose'); ?>

<?php get_template_part( 'parts/testimonials', 'home' ); ?>

<?php get_template_part( 'parts/blog' ); ?>

<?php get_footer(); ?>