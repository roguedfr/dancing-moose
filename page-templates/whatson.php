<?php
/**
 * ============== Template Name: What's On
 */
get_header();?>

<?php $heroImage = get_field('hero_image');?>
<section>
	<div class="h75 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		<div class="grid-container hero-carousel__content">
		  	<div class="grid-x text-center align-middle h75">
		  		<div class="cell small-offset-2 small-8">
		  			<h2 class="heading heading__white heading__xxl heading__alt-font pt2"><?php the_title();?></h2>
		  			<h6 class="heading heading__white heading__md heading__body-font pb2"><?php the_field('hero_content');?></h6>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section pb5 pt5">
	<div class="grid-container">
		<div class="grid-x text-center grid-margin-x">
			<?php
		    $loop = new WP_Query( array( 'post_type' => 'whats-on', 'orderby'=>'title','order'=>'ASC' ) );
		    if ( $loop->have_posts() ) :
		        while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="cell small-12 xlarge-6 grid-x grid-margin-x pb5 serpentine slide-up" data-equalize="location">
				<div class="cell small-12 medium-6 <?php if (($loop->current_post % 4 == 3) || ($loop->current_post % 4 == 2)) : ?>large-order-2<?php else: ?>small-order-1<?php endif ?>">
					<h3 class="heading heading__lg heading__alt-font heading__primary text-center pb1"><?php echo get_the_title();?></h3>
	 					<div data-equalizer-watch="location"><?php the_field('hero_content');?></div>
						<a href="<?php the_permalink();?>" class="hero-carousel__button btn btn__red btn__version1"><?php get_template_part( 'assets/svg/button1' ); ?><span class="heading heading__md heading__caps"><?php the_field('external_button_text');?></span></a>
				</div>
				<div class="cell small-12 medium-6 show-for-medium <?php if (($loop->current_post % 4 == 3) || ($loop->current_post % 4 == 2)) : ?>large-order-1<?php else: ?>small-order-2<?php endif ?>">
					<?php $image = get_field('hero_image');?>
					<div class="imageDiv imageDiv__square <?php if (($loop->current_post % 4 == 3) || ($loop->current_post % 4 == 2)) : ?>imageDiv__rotate-alt<?php else: ?>imageDiv__rotate<?php endif ?> imageDiv__border-thick imageDiv__shadow" style="background-image:url(<?php echo $image['url'];?>);">
					</div>
				</div>
			</div>
			<?php endwhile; endif; wp_reset_postdata();?>
		</div>
	</div>
</section>

<?php get_template_part( 'parts/choose-moose'); ?>

<?php get_footer(); ?>