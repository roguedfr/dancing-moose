<?php get_header();?>
<?php $heroImage = get_field('hero_image');?>
<?php $stampLogo = get_field('dancing_moose_logo');?>
<section>
	<div class="h75 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		<div class="grid-container hero-carousel__content">
		  	<div class="grid-x text-center align-middle h75">
		  		<div class="cell">
		  			<h2 class="heading heading__white heading__xxl heading__alt-font pt2"><?php the_title();?></h2>
		  			<h6 class="heading heading__white heading__md heading__body-font pb2"><?php the_field('location');?> | <a href="tel:<?php the_field('phone_number');?>" class="heading__white"><?php the_field('phone_number');?></a> | <a href="mailto:<?php the_field('email_address');?>" class="heading__white"><?php the_field('email_address');?></a></h6>
		  			<?php if( have_rows('booking_button') ):
						while( have_rows('booking_button') ): the_row(); ?>
							<?php get_template_part( 'parts/_button' ); ?>
					<?php endwhile; endif;?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="pt7 pb7">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 xlarge-3 large-4 medium-offset-0 large-offset-1 text-center hours-stamp">
				<img src="<?php echo $stampLogo['url'];?>" class="hours-stamp__image"/>
				<h4 class="heading heading__lg heading__caps font700 pb2 pt2">Opening Hours</h4>
				<?php if( have_rows('opening_times_list') ):
 					while( have_rows('opening_times_list') ): the_row(); ?>
 					<h5 class="heading heading__md heading__body-font font700 pb1"><?php the_sub_field('days');?></h5>
 					<h5 class="heading heading__md heading__body-font pb1"><?php the_sub_field('time');?></h5>
 				<?php endwhile; endif;?>
			</div>
			<div class="cell small-12 medium-8 large-7 xlarge-8">
				<div class="pb2"><?php the_field('content');?></div>
			</div>
			<div class="cell small-12">
				<?php 
				$location = get_field('map_location');
				if( $location ): ?>
				    <div class="acf-map" data-zoom="16">
				        <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
				    </div>
				<?php endif; ?>

				<?php 
				$images = get_field('gallery');
				if( $images ): ?>
				    <div class="locationGallery pt4">
				        <?php foreach( $images as $image ): ?>
			                <a href="<?php echo esc_url($image['url']); ?>" class="locationGallery__image">
			                     <img src="<?php echo esc_url($image['sizes']['gallery-thumbnails']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			                </a>
				        <?php endforeach; ?>
				    </div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>

<script type="text/javascript">
(function( $ ) {

/**
 * initMap
 *
 * Renders a Google Map onto the selected jQuery element
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @return  object The map instance.
 */
function initMap( $el ) {

    // Find marker elements within map.
    var $markers = $el.find('.marker');

    // Create gerenic map.
    var mapArgs = {
        zoom        : $el.data('zoom') || 16,
        mapTypeId   : google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map( $el[0], mapArgs );

    // Add markers.
    map.markers = [];
    $markers.each(function(){
        initMarker( $(this), map );
    });

    // Center map based on markers.
    centerMap( map );

    // Return map instance.
    return map;
}

/**
 * initMarker
 *
 * Creates a marker for the given jQuery element and map.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @param   object The map instance.
 * @return  object The marker instance.
 */
function initMarker( $marker, map ) {

    // Get position from marker.
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var latLng = {
        lat: parseFloat( lat ),
        lng: parseFloat( lng )
    };

    // Create marker instance.
    var marker = new google.maps.Marker({
        position : latLng,
        map: map
    });

    // Append to reference for later use.
    map.markers.push( marker );

    // If marker contains HTML, add it to an infoWindow.
    if( $marker.html() ){

        // Create info window.
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });

        // Show info window when marker is clicked.
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open( map, marker );
        });
    }
}

/**
 * centerMap
 *
 * Centers the map showing all markers in view.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   object The map instance.
 * @return  void
 */
function centerMap( map ) {

    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function( marker ){
        bounds.extend({
            lat: marker.position.lat(),
            lng: marker.position.lng()
        });
    });

    // Case: Single marker.
    if( map.markers.length == 1 ){
        map.setCenter( bounds.getCenter() );

    // Case: Multiple markers.
    } else{
        map.fitBounds( bounds );
    }
}

// Render maps on page load.
$(document).ready(function(){
    $('.acf-map').each(function(){
        var map = initMap( $(this) );
    });
});

})(jQuery);
</script>