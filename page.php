<?php 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>

<?php $heroImage = get_field('hero_image');?>
<section>
	<div class="h50 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		<div class="grid-container hero-carousel__content">
		  	<div class="grid-x text-center align-middle h50">
		  		<div class="cell small-offset-2 small-8">
		  			<h2 class="heading heading__white heading__xxl heading__alt-font pt2"><?php the_title();?></h2>
		  			<h6 class="heading heading__white heading__md heading__body-font pb2"><?php the_field('hero_content');?></h6>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pt7 pb7">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12">
				<div class="pb2"><?php the_field('content');?></div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>