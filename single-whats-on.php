<?php get_header();?>

<?php $heroImage = get_field('hero_image');?>
<section>
	<div class="h75 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		<div class="grid-container hero-carousel__content">
		  	<div class="grid-x text-center align-middle h75">
		  		<div class="cell small-offset-2 small-8">
		  			<h2 class="heading heading__white heading__xxl heading__alt-font pt2"><?php the_title();?></h2>
		  			<h6 class="heading heading__white heading__md heading__body-font pb2"><?php the_field('hero_content');?></h6>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pt7 pb7">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12">
				<div class="pb2"><?php the_field('content');?></div>
			</div>
			<div class="cell small-12 grid-x grid-padding-x small-up-4">
				<?php if( have_rows('booking_buttons') ):
					while( have_rows('booking_buttons') ): the_row(); ?>
						<div class="cell"><?php get_template_part( 'parts/_button' ); ?></div>
				<?php endwhile; endif;?>
			</div>
			<div class="cell small-12">
				<?php 
				$images = get_field('gallery');
				if( $images ): ?>
				    <div class="locationGallery pt4">
				        <?php foreach( $images as $image ): ?>
			                <a href="<?php echo esc_url($image['url']); ?>" class="locationGallery__image">
			                     <img src="<?php echo esc_url($image['sizes']['gallery-thumbnails']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			                </a>
				        <?php endforeach; ?>
				    </div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_template_part( 'parts/choose-moose'); ?>

<?php get_footer(); ?>