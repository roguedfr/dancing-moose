<section class="pt2 pb2 section__primary section">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 medium-6 text-right text-sm-center">
 				<h3 class="heading heading__lg heading__caps heading__white"><?php the_field('fyn_title');?></h3>
 			</div>
 			<div class="cell small-12 medium-6 text-left text-sm-center">
 				<?php if( have_rows('fyn_button') ):
					while( have_rows('fyn_button') ): the_row(); ?>
						<?php $buttonColour = get_sub_field('colour');?>
						<?php $buttonVersion = get_sub_field('button_type');?>
 						<a data-open="exampleModal1" class="hero-carousel__button btn btn__<?php echo $buttonColour;?> btn__version<?php echo $buttonVersion;?>"><?php get_template_part( 'assets/svg/button' . $buttonVersion ); ?><span class="heading heading__md heading__caps"><?php the_sub_field('title');?></span></a>
				<?php endwhile; endif;?>
			</div>
		</div>
	</div>
</section>

<div class="large reveal" id="exampleModal1" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
  <?php echo do_shortcode("[wpsl]"); ?>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>