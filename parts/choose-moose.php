<section class="section pb5">
	<div class="grid-container grid-x text-center">
		<div class="cell small-12 grid-x grid-margin-x" id="book-now">
			<div class="small-12 large-3 cell">
		      <label for="right-label" class="text-center text-lg-right middle heading heading__lg heading__caps heading__alt-font heading__primary">Choose Your Moose</label>
		    </div>
		    <div class="small-12 medium-8 large-6 cell">
			  <select class="chooseSelect">
				<?php
			    $loop = new WP_Query( array( 'post_type' => 'location', 'orderby'=>'title','order'=>'ASC' ) );
			    if ( $loop->have_posts() ) :
			        while ( $loop->have_posts() ) : $loop->the_post(); ?>
			        	<?php if( have_rows('booking_button') ):
 							while( have_rows('booking_button') ): the_row(); ?>
			    			<option value="<?php the_sub_field('link');?>"><?php echo get_the_title();?></option>
			    		<?php endwhile; endif;?>
			    <?php endwhile; endif; wp_reset_postdata();?>
			  </select>
			</div>
			<div class="small-12 medium-4 large-3 cell text-center text-md-left">
				<a href="#" class="hero-carousel__button btn btn__red btn__version3" id="choose-moose"><?php get_template_part( 'assets/svg/button3'); ?><span class="heading heading__md heading__caps">Book now</span></a>
			</div>
		</div>
	</div>
</section>