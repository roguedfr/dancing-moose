<?php if( have_rows('video_section') ):
 while( have_rows('video_section') ): the_row(); ?>
 	<section class="section section__black section__white-text pb5 pt5">
 		<div class="grid-container grid-x text-center">
 			<div class="cell small-offset-2 small-8 pb2 slow-fade">
 				<h2 class="heading heading__white heading__xl heading__caps pb2"><?php the_sub_field('title');?></h2>
 				<?php the_sub_field('content');?>
 			</div>
 			<?php if(get_sub_field('video_url')){;?>
 			<div class="cell small-offset-2 small-8 pb2 slow-fade">
 				<div class="responsive-embed widescreen">
 					<?php the_sub_field('video_url');?>
 				</div>
 			</div>
 			<?php };?>
 			<div class="cell slide-up">
 				<?php if( have_rows('button') ):
					while( have_rows('button') ): the_row(); ?>
						<?php get_template_part( 'parts/_button' ); ?>
				<?php endwhile; endif;?>
 			</div>
 		</div>
 	</section>
 <?php endwhile; endif;?>