<section class="pt5 pb5 <?php if(!is_page_template('page-templates/home.php')){;?>section__black<?php };?>">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 text-center">
				<h3 class="heading heading__xl <?php if(!is_page_template('page-templates/home.php')){;?>heading__white<?php };?> heading__caps pb3 slow-fade">
					The Latest from the Moose
				</h3>
			</div>
			<?php
	    		$loop = new WP_Query( array( 
	    			'post_type' => 'post',
	    			'posts_per_page' => 3,
	    			'post__not_in' => array(get_the_ID()) ) );
	    		if ( $loop->have_posts() ) :
	        	while ( $loop->have_posts() ) : $loop->the_post(); ?>
	        		<?php $image = get_field('hero_image');?>
				<div class="cell medium-4 small-12 blog__container slide-up mt1">
					<a href="<?php the_permalink();?>" class="blog ">
						<div class="blog__background" style="background-image: url(<?php echo $image['url'];?>)"></div>
						<h4 class="heading heading__xl heading__white heading__caps pb3 pt3 pl4 pr2 text-center blog__title"><?php the_title();?></h4>
						<div class="blog__content pl1 pr1 text-center">
							<?php the_field('hero_content');?>
						</div>
						<span class="hero-carousel__button btn btn__black btn__version3 blog__button"><?php get_template_part( 'assets/svg/button3' ); ?><span class="heading heading__md heading__caps">Read More</span></span>
					</a>
				</div>
			<?php endwhile; endif;?>
			<div class="cell small-12 text-center pt3 slide-up">
				<a href="<?php the_field('blog_page', 'options');?>"><span class="hero-carousel__button btn btn__red btn__version4"><?php get_template_part( 'assets/svg/button4' ); ?><span class="heading heading__md heading__caps">All News</span></span></a>
			</div>
		</div>
	</div>
</section>


