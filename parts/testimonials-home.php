<section class="pb2 pt2 section">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 section section__white section__shadow pb2 pt2">
				<div class="grid-x grid-margin-x">
					<div class="cell small-12 text-center">
						<h3 class="heading heading__xl heading__caps pb1">
							<?php the_field('testimonial_title');?>
						</h3>
					</div>
					<?php if( have_rows('testimonials') ):?>
					<div class="testimonial_carousel cell small-12 grid-x grid-margin-x">
						<?php while( have_rows('testimonials') ): the_row(); ?>
							<div class="testimonial_carousel__container cell small-12">
								<div class="section__primary pt2 pb2 pr2 pl2 testimonial_carousel__skew">
									<div class="heading heading__lg heading__caps heading__heading-font text-center heading__white"><?php the_sub_field('content');?></div>
								</div>
								<div class="heading heading__caps text-right heading__sm pr2 pt2"><?php the_sub_field('reviewer');?></div>
							</div>
 						<?php endwhile;?>
					</div>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</section>