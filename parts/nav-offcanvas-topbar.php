<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="top-bar" id="top-bar-menu">
	<div class="show-for-large" id="main-nav-container" data-smooth-scroll data-offset="100">
		<?php joints_top_nav_left(); ?>
		<ul class="menu">
			<li><a href="<?php echo home_url(); ?>" id="main-logo"><?php get_template_part( '/assets/svg/logo' ); ?></a></li>
		</ul>	
		<?php joints_top_nav_right(); ?>
		<div class="top-bar__button" data-smooth-scroll data-offset="100">
				<?php if( have_rows('book_now_button', 'options') ):
				while( have_rows('book_now_button', 'options') ): the_row(); ?>
					<?php $buttonColour = get_sub_field('colour');?>
					<?php $buttonVersion = get_sub_field('button_type');?>
					 <a class="hero-carousel__button btn btn__<?php echo $buttonColour;?> btn__version<?php echo $buttonVersion;?>" data-open="bookingModal"><?php get_template_part( 'assets/svg/button' . $buttonVersion ); ?><span class="heading heading__md heading__caps"><?php the_sub_field('title');?></span></a>
			<?php endwhile; endif;?>
		</div>	
	</div>
	<div class="top-bar-right float-right hide-for-large">
		<ul class="menu">
			<!-- <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li> -->
			<li><a class="off-canvas__toggle" data-toggle="off-canvas"><i class="fas fa-bars"></i></a></li>
		</ul>
	</div>
</div>