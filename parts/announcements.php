<?php if( have_rows('announcements', 'options') &&  (get_field('active_announcements', 'options') == true)):?>
<section class="section section__black" id="announcements" data-closable>
	<div class="grid-container fluid">
		<div class="grid-x grid-padding-x">
			<div class="small-offset-1 small-10 text-center cell announcements-carousel">
		 		<?php while( have_rows('announcements', 'options') ): the_row(); ?>
		 				<div class="remove-padding"><?php the_sub_field('title');?></div>
		 		<?php endwhile; ?>
			</div>
			<button class="close-button" aria-label="Close alert" type="button" data-close>
		    	<span aria-hidden="true">&times;</span>
		  	</button>
		</div>
	</div>
</section>
<?php endif;?>