<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="off-canvas position-right" id="off-canvas" data-off-canvas>
	<button class="close-button" aria-label="Close menu" type="button" data-close>
	  <span aria-hidden="true">&times;</span>
	</button>
	
	<div class="grid-x gridd-padding-x">
	
		<div class="text-center small-12 cell">
		
			<div class="off-canvas__moose float-center pt3 pb2">
			
				<?php get_template_part( 'assets/svg/moose-head' ); ?>

			</div>
		
		</div>

		<div class="text-center small-12 cell off-canvas__links" data-smooth-scroll data-offset="20">

			<?php joints_off_canvas_nav(); ?>

			<div >
					<?php if( have_rows('book_now_button', 'options') ):
					while( have_rows('book_now_button', 'options') ): the_row(); ?>
						<?php get_template_part( 'parts/_button' ); ?>
				<?php endwhile; endif;?>
			</div>

		</div>

	</div>

	<?php if ( is_active_sidebar( 'offcanvas' ) ) : ?>

		<?php dynamic_sidebar( 'offcanvas' ); ?>

	<?php endif; ?>

</div>
