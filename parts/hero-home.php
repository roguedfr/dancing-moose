<?php if( have_rows('hero') ):
 while( have_rows('hero') ): the_row(); ?>
	<?php if( have_rows('hero_slider') ):?>
	    <section class="hero-carousel">
	    <?php while( have_rows('hero_slider') ): the_row(); ?>
	    	<?php $heroImage = get_sub_field('hero_image');?>
		  <div class="h100 hero-carousel__container" style="background-image:url('<?php echo $heroImage['url'];?>')">
		  	<div class="grid-container hero-carousel__content">
		  		<div class="grid-x text-center align-middle h100">
		  			<div class="cell">
		  				<div class="hero-carousel__moose"><?php get_template_part( 'assets/svg/moose-head' ); ?></div>
		  				<h1 class="heading heading__white heading__xxl heading__alt-font pb1 pt2"><?php the_sub_field('hero_heading');?></h1>
		  				<h2 class="heading heading__white heading__lg heading__heading-font pb2"><?php the_sub_field('hero_sub_heading');?></h2>
		  				<div class="hero-carousel__buttons" data-smooth-scroll data-offset="100">
			  				<?php if( have_rows('hero_button_1') ):
	 							while( have_rows('hero_button_1') ): the_row(); ?>
	 								<?php get_template_part( 'parts/_button' ); ?>
	 						<?php endwhile; endif;?>
	 						<?php if( have_rows('hero_button_2') ):
	 							while( have_rows('hero_button_2') ): the_row(); ?>
	 								<?php get_template_part( 'parts/_button' ); ?>
	 						<?php endwhile; endif;?>
	 					</div>
		  			</div>
		  		</div>
		  	</div>
		  </div>
	  <?php endwhile;?>
	</section>
	<?php endif;?>
<?php endwhile; endif;?>