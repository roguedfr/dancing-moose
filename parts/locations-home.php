
<section class="section pb5 pt5">
	<div class="grid-container">
		<div class="grid-x text-center grid-margin-x">
			<?php if( have_rows('our_locations') ):
	 		while( have_rows('our_locations') ): the_row(); ?>
			<div class="cell small-12 medium-offset-2 medium-8 pb2 slow-fade">
				<h2 class="heading heading__primary heading__xl heading__caps pb2 moose moose__red">
					<?php get_template_part( 'assets/svg/moose-head' ); ?>
						<?php the_sub_field('title');?>
					<?php get_template_part( 'assets/svg/moose-head' ); ?>
				</h2>
				<?php the_sub_field('content');?>
			</div>
			<?php endwhile; endif;?>
			<?php
		    $loop = new WP_Query( array( 'post_type' => 'location', 'orderby'=>'title','order'=>'ASC' ) );
		    if ( $loop->have_posts() ) :
		        while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="cell small-12 xlarge-6 grid-x grid-margin-x pb5 serpentine slide-up" data-equalize="location">
				<div class="cell small-12 medium-6 <?php if (($loop->current_post % 4 == 3) || ($loop->current_post % 4 == 2)) : ?>large-order-2<?php else: ?>small-order-1<?php endif ?>">
					<h3 class="heading heading__lg heading__alt-font heading__primary text-center pb1"><?php echo get_the_title();?></h3>
					<?php if( have_rows('external_page_info') ):
	 				while( have_rows('external_page_info') ): the_row(); ?>
	 					<div data-equalizer-watch="location"><?php the_sub_field('excerpt');?></div>
	 					<?php if(!is_page_template('page-templates/book-now.php')){;?>
	 					<?php if( have_rows('button') ):
		 				while( have_rows('button') ): the_row(); ?>
		 					<?php $buttonColour = get_sub_field('colour');?>
							<?php $buttonVersion = get_sub_field('button_type');?>
							
	 						<a href="<?php the_permalink();?>" class="hero-carousel__button btn btn__<?php echo $buttonColour;?> btn__version<?php echo $buttonVersion;?>"><?php get_template_part( 'assets/svg/button' . $buttonVersion ); ?><span class="heading heading__md heading__caps">Check out <?php echo get_the_title();?></span></a>
	 						
		 				<?php endwhile; endif;?>
		 				<?php } else {;?>
		 					<?php if( have_rows('booking_button') ):
		 				while( have_rows('booking_button') ): the_row(); ?>
		 					<?php $buttonColour = get_sub_field('colour');?>
							<?php $buttonVersion = get_sub_field('button_type');?>
							
	 						<a href="<?php the_permalink();?>" class="hero-carousel__button btn btn__<?php echo $buttonColour;?> btn__version<?php echo $buttonVersion;?>"><?php get_template_part( 'assets/svg/button' . $buttonVersion ); ?><span class="heading heading__md heading__caps">Book at <?php echo get_the_title();?></span></a>
	 						<?php endwhile; endif;?>
	 					<?php };?>
	 				<?php endwhile; endif;?>
				</div>
				<div class="cell small-12 medium-6 show-for-medium <?php if (($loop->current_post % 4 == 3) || ($loop->current_post % 4 == 2)) : ?>large-order-1<?php else: ?>small-order-2<?php endif ?>">
					<?php if( have_rows('external_page_info') ):
	 				while( have_rows('external_page_info') ): the_row(); ?>
	 					<?php $image = get_sub_field('image');?>
	 					<div class="imageDiv imageDiv__square <?php if (($loop->current_post % 4 == 3) || ($loop->current_post % 4 == 2)) : ?>imageDiv__rotate-alt<?php else: ?>imageDiv__rotate<?php endif ?> imageDiv__border-thick imageDiv__shadow" style="background-image:url(<?php echo $image['url'];?>);">
	 					</div>
	 				<?php endwhile; endif;?>
				</div>
			</div>
			<?php endwhile; endif; wp_reset_postdata();?>
		</div>
	</div>
</section>


  