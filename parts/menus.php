<section class="pt5 pb5" id="menus">
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell small-12 text-center">
				<h3 class="heading heading__xl heading__caps pb1 slow-fade">
					<?php the_field('title', 'options');?>
				</h3>
			</div>
			<?php if( have_rows('food_category', 'options') ):?>
			<div class="cell small-12 show-for-medium">
					<ul class="tabs grid-x" data-tabs id="menu-tabs">
 					<?php while( have_rows('food_category', 'options') ): the_row(); ?>
						<li class="cell auto tabs-title <?php if (get_row_index() == 1):?>is-active<?php endif;?>"><a href="#menu<?php echo get_row_index(); ?>" aria-selected="true"><?php the_sub_field('title');?></a></li>
				  	<?php endwhile;?>
				</ul>
				<?php while( have_rows('food_category', 'options') ): the_row(); ?>
				<div class="tabs-content" data-tabs-content="menu-tabs">
					<div class="tabs-panel <?php if (get_row_index() == 1):?>is-active<?php endif;?>" id="menu<?php echo get_row_index(); ?>">
				    	<?php if( have_rows('meal_time', 'options') ):?>
				    		<?php while( have_rows('meal_time', 'options') ): the_row(); ?>
				    		<div class="grid-x grid-padding-x text-center">
				    			<div class="cell small-12">
					    			<div class="heading heading__caps heading__lg heading__alt-font pb1 slow-fade"><?php the_sub_field('time');?></div>
					    		</div>
				    		</div>
				    		<div class="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3">
				    			<?php if( have_rows('meal', 'options') ):?>
				    				<?php while( have_rows('meal', 'options') ): the_row(); ?>
				    				<div class="cell grid-x grid-padding-x pb2 meal__container slow-fade">
				    					<div class="cell small-8">
				    						<h4 class="heading heading__md heading__caps"><?php the_sub_field('name');?></h4>
				    						<?php the_sub_field('description');?>
				    					</div>
				    					<div class="cell small-4">
				    						<div class="heading heading__md heading__caps heading__heading-font">£<?php the_sub_field('price');?></div>
				    					</div>
				    				</div>
				    				<?php endwhile;?>
				    			<?php endif;?>
				    		</div>
				    		<?php endwhile;?>
				    	<?php endif;?>
				  	</div>
				</div>
				<?php endwhile;?>
			</div>
			<div class="cell small-12 hide-for-medium">
				<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
					<?php while( have_rows('food_category', 'options') ): the_row(); ?>
				  		<li class="accordion-item" data-accordion-item>
					    	<a href="#" class="accordion-title"><?php the_sub_field('title');?></a>

						    <div class="accordion-content" data-tab-content>
						    	<?php if( have_rows('meal_time', 'options') ):?>
						    		<?php while( have_rows('meal_time', 'options') ): the_row(); ?>
						    		<div class="grid-x grid-padding-x text-center">
						    			<div class="cell small-12">
							    			<div class="heading heading__caps heading__lg heading__alt-font pb1 slow-fade"><?php the_sub_field('time');?></div>
							    		</div>
						    		</div>
						    		<div class="grid-x grid-padding-x small-up-1 medium-up-2 large-up-3">
						    			<?php if( have_rows('meal', 'options') ):?>
						    				<?php while( have_rows('meal', 'options') ): the_row(); ?>
						    				<div class="cell grid-x grid-padding-x pb2 meal__container slow-fade mb1">
						    					<div class="cell small-8">
						    						<h4 class="heading heading__md heading__caps"><?php the_sub_field('name');?></h4>
						    					</div>
						    					<div class="cell small-4">
						    						<div class="heading heading__md heading__caps heading__heading-font">£<?php the_sub_field('price');?></div>
						    					</div>
						    					<div class="cell small-12">
						    						<?php the_sub_field('description');?>
						    					</div>
						    				</div>
						    				<?php endwhile;?>
						    			<?php endif;?>
						    		</div>
						    		<?php endwhile;?>
						    	<?php endif;?>
						    </div>
				  		</li>
				  	<?php endwhile;?>
				</ul>
			</div>
			<?php endif;?>
		</div>
	</div>
</section>