<section class="pt2 pb2">
	<div class="grid-container">
		<div class="grid-x grid-margin-x offers-carousel__container">
			<div class="offers-carousel__moose offers-carousel__moose-left pb1 pt1"><?php get_template_part( 'assets/svg/moose-head' ); ?></div>
			<div class="cell small-12 text-center offers-carousel">
				<?php if( have_rows('offer', 'options') ):
 				while( have_rows('offer', 'options') ): the_row(); ?>
 					<?php if( get_sub_field('active') == true ): ?>
 					<h3 class="heading heading__lg heading__caps remove-padding"><?php the_sub_field('offers_title');?></h3>
 				<?php endif;?>
 				<?php endwhile; endif;?>
			</div>
			<div class="offers-carousel__moose offers-carousel__moose-right pb1 pt1"><?php get_template_part( 'assets/svg/moose-head' ); ?></div>
		</div>
	</div>
</section>